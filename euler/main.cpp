#include <iostream>
using std::cin;

#include "euler.h"

int main()
{
	//pretty simple. Initialize a graph, and some edges, find a eulerian circuit
	int v1, v2;
	graph g;

	//reads from stdin. stdin should be redirected to a file containing edge info
	/*
	*v_i v_j
	*v_x v_y
	*.
	*.
	*.
	*/
	while (cin >> v1)
	{
		cin >> v2;
		g.addEdge(v1, v2);
	}
	g.eulercircuit();
}