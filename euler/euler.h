#pragma once
#include <iostream>
using std::cout;
#include <cstring>
#include <string>
using std::string;
#include <list>
using std::list;
#include <algorithm>
#include <stdlib.h>
#include <sstream>

//for string + (string)int
template <typename T>
std::string NumberToString(T);

//forward declarations
class vertex;
class edge;
class graph;

//makes adding a vertex to a string more intuitive
//string& path will be modified
void operator+(vertex&, string&);

class vertex{
public:
	//public for ease of use
	list<edge*> edges;

	vertex();
	vertex(int);
#if 0 //caused a problem I don't want to fix right now
	~vertex();
#endif
	/*accessor functions*/

	bool wasVisited();
	int Val();

	//adds an edge pointer to list<edge*> edges
	void addEdge(edge*);

	//resets visited
	void clear();

private:
	bool visited;
	int value;
};









class edge{
public:
	vertex *v1;
	vertex *v2;
	
	//what edge has no vertices?
	edge();
	edge(vertex*, vertex*);

	//returns whether this edge has been traversed or not
	bool wastraversed();

	//should be used from a vertex. sets traversed and returns a pointer
	//to the vertex at the other end of the edge
	vertex* traverse(vertex*);

	//resets traversed
	void clear();

private:
	bool traversed;
};

class graph{
private:
	list<vertex> V;
	list<edge> E;
public:
	//adds an edge to the graph and any needed vertices if they don't exist yet
	void addEdge(int, int);
	//just adds a vertex
	void addVertex(int);
	//resets the graph so no vertex is considered visited and no edge is traversed yet
	void clear();

	//print a eulerian cycle on this graph
	void eulercircuit();
	//does the actual work to find euler circuit
	void eulercircuit(vertex*, string&);
};

