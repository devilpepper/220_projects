#include "euler.h"

//this just returns a string containing t
template <typename T>
std::string TtoString(T t)
{
	std::ostringstream ss;
	ss << t;
	return ss.str();
}

//makes a nice path string. When this is not the first vertex in
//the path, an arrow is placed between this vertex and the rest of the path
void operator+(vertex& v, string& s)
{
	if(s.length() > 0) s = " -> " + s;
	s = "v_" + TtoString(v.Val()) +s;
}

/*Vertex stuff*/

vertex::vertex()
{
	this->value = 0;
	this->visited = false;
}
vertex::vertex(int x)
{
	this->value = x;
	this->visited = false;
}
#if 0
vertex::~vertex()
{
	vertex* v;
	for (list<edge*>::iterator i = this->edges.begin(); i != this->edges.end(); i++)
	{
		v = (*i)->traverse(this);
		v->edges.remove(*i);
		this->edges.remove(*i);
		//delete (*i); //no idea why this is wrong
	}
}
#endif

bool vertex::wasVisited()
{
	return this->visited;
}
int vertex::Val()
{
	return this->value;
}
void vertex::addEdge(edge* e)
{
	this->edges.push_back(e);
}



void vertex::clear()
{
	this->visited = false;
}

/*edge stuff*/

edge::edge()
{
	//what a strange edge to have
	this->v1 = 0;
	this->v2 = 0;
	//probably never will be traversed
	this->traversed = false;
}
edge::edge(vertex* v_1, vertex* v_2)
{
	this->v1 = v_1;
	this->v2 = v_2;
	this->traversed = false;
}

bool edge::wastraversed()
{
	return this->traversed;
}

vertex* edge::traverse(vertex* v)
{
	vertex * u = 0;
	//if v is v2 on the edge, return v1
	//if v is v1 on the edge, return v2
	if (v == this->v2) u = this->v1;
	else if (v == this->v1) u = this->v2;
	//maybe v is a vertex not connected to this edge
	if (u != 0) this->traversed = true;
	return u;
}
void edge::clear()
{
	this->traversed = false;
}

/*graph*/

void graph::addEdge(int v1, int v2)
{
	vertex *p2v1 = 0;
	vertex *p2v2 = 0;
	edge* p2e = 0;
	//look for v1 and v2 in the graph's vertex list
	for (list<vertex>::iterator v = this->V.begin();
						((v != this->V.end()) && !(p2v1 && p2v2)); v++)
	{
		if (v->Val() == v1) p2v1 = &(*v);
		if (v->Val() == v2) p2v2 = &(*v);
	}
	//if any vertex was not found, create it
	if (!p2v1)
	{
		this->V.push_back(v1);
		p2v1 = &(this->V.back());
	}
	if (!p2v2)
	{
		this->V.push_back(v2);
		p2v2 = &(this->V.back());
	}
	//now make the new edge using the pointers to the vertices
	this->E.push_back(edge(p2v1,p2v2));
	p2e = &(this->E.back());
	//also add a pointer to the new edge to both of the vertices that touch it
	p2v1->addEdge(p2e);
	p2v2->addEdge(p2e);
}
void graph::addVertex(int v)
{
	this->V.push_back(v);
}
void graph::clear()
{
	//no vertex is considered "visited" anymore
	for (list<vertex>::iterator v = this->V.begin(); v != this->V.end(); v++)
		v->clear();
	//no edge is considered "traversed" anymore
	for (list<edge>::iterator e = this->E.begin(); e != this->E.end(); e++)
		e->clear();
}
	
void graph::eulercircuit()
{
	//start at some vertex, the first one is good,
	//find the Eulerian circuit,
	//and output it to stdout
	string path = "";
	vertex* v = &(this->V.front());
	eulercircuit(v, path);
	cout << path << std::endl;
}

void graph::eulercircuit(vertex* v, string& path)
{
	//this simple divide and conquer algorith finds a cycle and then trys to
	//find other cycles using untraversed edged
	//this is done by simply traversing all untraversed edges
	//you can't possible get stuck because the vertex you are visiting will
	//have an untraversed edge to leave from since all vertices have even degree.
	for (list<edge*>::iterator e = v->edges.begin(); e != v->edges.end(); e++)
		if (!(*e)->wastraversed()) eulercircuit((*e)->traverse(v), path);
	return ((*v) + path);
}