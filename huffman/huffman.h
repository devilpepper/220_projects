#include<iostream>
using std::cin;
using std::cout;
#include<cstring>
#include<string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::swap;
#include <map>
using std::map;

#define LEFT(x)  ((x<<1)+1)
#define RIGHT(x) ((x+1)<<1)
#define PARENT(x) ((x-1)/2)

struct hnode
{
	char c;
	int weight;
	hnode *left;
	hnode *right;
	hnode(){left = right = 0;}
};

//comparison funtion for hnode*s
bool lt(const hnode*, const hnode*);

/*need to sort the list in a heap format*/
void heapify_down(vector<hnode*>&, int);
void heapify_up(vector<hnode*>&, int);
void buildHeap(vector<hnode*>&);

/*priority queue stuff*/
hnode* pqPop(vector<hnode*>&);
void pqPush(vector<hnode*>&, hnode);

//returns the root of a huffman tree
hnode* huffman(char*, int);
//deletes huffman tree bottom up
void deleteHuffman(hnode*);

//outputs trie to stdout. Also make a map for use in encoding
void tree2trie(hnode*, map<char, std::string>&);
//called by tree2trie and does the actual work
void tree2trie(hnode*, map<char, std::string>&, std::string&);
//retrieves trie from stdin
void file2trie(map<std::string, char>&);
//compress the input file using huffman tree map
void hcompress(char*, int, map<char, std::string>&);
//decompress the input file using huffman tree map
void dcompress(char*, int, map<std::string, char>&);