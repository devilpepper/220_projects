/*
To compile in Windows,
git clone https://github.com/skandhurkat/Getopt-for-Visual-Studio
Add the git repo to include directories
In VC++, project properties -> Configuration Properties -> C/C++ -> General 
														: Additional Include Directories
*/
#include<iostream>
using std::cin;
using std::cout;
#include<cstring>
#include<string>
using std::string;
#include <stdlib.h> /* atoi function. */
#include <getopt.h>
#include<vector>
using std::vector;
#include <fstream>
using std::fstream;

#include "huffman.h"

static const char* usage =
"Usage: %1$s [OPTIONS]...\n"
"General options:\n"
"   --compress  FILE    Compress input file.\n"
"   --decompress FILE   Decompress input file.\n"
"   -h,--help           show this message and exit.\n"
"Suggested use:\n"
"   %1$s --compress   FILETOCOMPRESS > TRIECODE\n"
"   %1$s --decompress COMPRESSEDFILE < TRIECODE\n";

int main(int argc, char *argv[])
{
	string argv0 = argv[0];
	if (argc != 3)
	{
		//MSVS fprintf() doesn't process positional specifiers like GNU fprintf()
		//Also, directory slashes are different. Here, I'm gettting the executable's name
		//_MBCS happens to be defined for VC++ projects
		//If it isn't and you are using MSVS, define it.
#ifdef _MBCS
		_fprintf_p(stderr, usage, argv0.substr(argv0.find_last_of('\\')+1).c_str());
#else
		fprintf(stderr, usage, argv0.substr(argv0.find_last_of('/') + 1).c_str());
#endif
		exit(1);
	}
	// define long options
	bool compr = 0, dcompr = 0;
	string fileName = "";
	static struct option long_opts[] = {
		{ "compress",   required_argument, 0, 'c' },
		{ "decompress", required_argument, 0, 'd' },
		{ "help",       no_argument,       0, 'h' },
		{ 0, 0, 0, 0 }
	};
	// process options:
	char c;
	int opt_index = 0;
	//options with required arguments need ':' after it for getopt
	while ((c = getopt_long(argc, argv, "c:d:h", long_opts, &opt_index)) != -1) {
		switch (c) {
		case 'c':
			compr = true;
			fileName = optarg;
			break;
		case 'd':
			dcompr = true;
			fileName = optarg;
			break;
		case 'h':
#ifdef _MBCS
			_fprintf_p(stderr, usage, argv0.substr(argv0.find_last_of('\\')+1).c_str());
#else
			fprintf(stderr, usage, argv0.substr(argv0.find_last_of('/') + 1).c_str());
#endif
			exit(0);
		case '?':
			
#ifdef _MBCS
			_fprintf_p(stderr, usage, argv0.substr(argv0.find_last_of('\\')+1).c_str());
#else
			fprintf(stderr, usage, argv0.substr(argv0.find_last_of('/')+1).c_str());
#endif
			exit(1);
		}
	}

	//open input file
	vector<char> data;
	std::ifstream inFile(fileName.c_str(), std::ios::binary | std::ios::ate);
	if (!inFile.is_open())
	{
#ifdef _MBCS
		_fprintf_p(stderr, usage, argv0.substr(argv0.find_last_of('\\')+1).c_str());
#else
		fprintf(stderr, usage, argv0.substr(argv0.find_last_of('/') + 1).c_str());
#endif
		exit(1);
	}
	
	//read input file to a char array
	data.resize(inFile.tellg());
	inFile.seekg(0, inFile.beg);
	inFile.read(&data[0], data.size());
	inFile.close();

	//for redirecting cout to ofstream
	std::streambuf *cout_buffer;
	
	

	

	
	if (compr)
	{
		hnode* nodes;
		data.push_back('\0');
		
		//make huffman tree
		nodes = huffman(&data[0], data.size());

		//get trie from tree
		map<char, std::string> trie;
		tree2trie(nodes, trie);

		//create new file to output to
		fileName = "compressed_" + fileName;
		std::ofstream outFile(fileName.c_str(), std::ios::binary);
		//make cout << stuff mean outFile << stuff
		cout_buffer = cout.rdbuf(outFile.rdbuf());

		//output file huffman encoded (originally written to stdout)
		hcompress(&data[0], data.size(), trie);
		//restore cout
		cout.rdbuf(cout_buffer);

		outFile.flush();
		outFile.close();
		//deleteHuffman(nodes);
	}
	else if (dcompr)
	{
		//create new file to output to
		fileName = "decompressed_" + fileName;
		std::ofstream outFile(fileName.c_str(), std::ios::binary);
		//make cout << stuff mean outFile << stuff
		cout_buffer = cout.rdbuf(outFile.rdbuf());

		//retrive trie from stdin
		map<std::string, char> trie;
		file2trie(trie);
		//output file decoded (originally written to stdout)
		dcompress(&data[0], data.size(), trie);
		//restore cout
		cout.rdbuf(cout_buffer);

		outFile.flush();
		outFile.close();
	}
	return 0;
}