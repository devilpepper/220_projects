#include "huffman.h"

bool lt(hnode* lhs, hnode* rhs)
{
	return (lhs->weight < rhs->weight);
}

/*need to sort the list in a heap format*/
void heapify_down(vector<hnode*>& A, int i)
{
	int n = A.size();
	if(LEFT(i) < n)//not leaf node
	{
		int child = LEFT(i);
		//RIGHT(i) = LEFT(i) + 1
		//if the right child exists and it is smaller than the left child
		//make the right child the one we want to swap
		if(child+1 < n && lt(A[child+1], A[child])) child++;
		//swap if the child is indeed smaller than this node and heapify down.
		if(lt(A[child],A[i]))
		{
			swap(A[i], A[child]);
			heapify_down(A, child);
		}
	}
}
void heapify_up(vector<hnode*>& A, int i)
{
	if(i != 0)//not root node
	{
		int parent = PARENT(i);
		//if this node is smaller than it's parent, swap and heapify up.
		if(lt(A[i], A[parent]))
		{
			swap(A[parent], A[i]);
			heapify_up(A, parent);
		}
	}
}
void buildHeap(vector<hnode*>&A)
{
	/* Note that all leaves are heaps on their own.  So, you can work
	 * backwards through the tree calling heapify, starting from the
	 * last node's parent. */
	int i = PARENT(A.size()-1); //start at last node's parent
	while (i >= 0) heapify_down(A, i--); //move left along the tree and heapify
}

/*priority queue stuff*/
hnode* pqPop(vector<hnode*>& Q)
{
	/* copy last value to top and reduce size (call pop_back). */
	
	hnode *value = Q[0]; //copy the top for return value
	Q[0] = Q.back(); //copy last value to top.
	Q.pop_back(); //pop_back().
	heapify_down(Q, 0); //heapify down from root node
	return value;
}
void pqPush(vector<hnode*>& Q, hnode* x)
{
	/* push new item to back; then heapify up. */
	Q.push_back(x); //O(n)... amortized constant runtime
					//If resize is necessary, it's the worst case.
	heapify_up(Q, (Q.size() - 1));
}

hnode* huffman(char* data, int numchar)
{
	map<char, int> f; //here's the frequency table and it's allocation
	for(int i=0; i<numchar; i++) f[data[i]]++;
	size_t n = f.size(), j=0; //"Note that n will suffice..." j is an iterator
	
	hnode *nodes = new hnode[n]; //allocate space for n hNodes

	vector<hnode*> pq;
	
	//set each symbol and it's frequency to the jth hNode's symbol and weight
	//push a pointer to the jth Node into the vector pq
	for(map<char,int>::iterator i=f.begin(); i!=f.end(); i++){
		nodes[j].c = i->first;
		nodes[j].weight = i->second;
		pq.push_back(&nodes[j++]);
		//std::cerr << (i->first) << "\t" << (i->second) << "\n";
	}

	//buid priority queue from pq
	buildHeap(pq); //T(n) = O(n)
	
	hnode* r;
	//While there are more than 1 pointer in pq,
	//pop the top 2 elements(smallest weight)
	//merge them together in one of the excess hNodes(the jth one)
	//set the new hNode's weight to the sum of it's children's weights
	//finally, push it back into the priority queue
	while(pq.size() > 1) //T(n) = O(n) because vector resize will not happen
	{
		r = new hnode();
		r->left = pqPop(pq);
		r->right = pqPop(pq);
		r->weight = r->left->weight + r->right->weight;
		pqPush(pq, r);
	}
	//in case we are making a huffman code out of nothing, return null, if so.
	if(pq.size() == 0) pq.push_back(0);
	return pq[0];
}
void deleteHuffman(hnode* doomed)
{
	if(doomed != 0)
	{
		//delete left and right first, then delete this
		deleteHuffman(doomed->left);
		deleteHuffman(doomed->right);
		delete doomed;
	}
}

void tree2trie(hnode* tree, map<char, std::string>& trie)
{
	//create a string to be passed around by recursive tree2trie()
	std::string code = "";
	tree2trie(tree, trie, code);
	//output trie to stdout (Redirect it to a file to save it. Needed for decompression)
	for(map<char, std::string>::iterator i=trie.begin(); i!=trie.end(); i++)
		cout<<(i->second)<<"\t"<<(i->first)<<"\n";
}

void tree2trie(hnode* tree, map<char, std::string>& trie, std::string& code)
{
	//if this is a leaf node, put the code in the map at this node's character index
	if((tree->left == 0) && (tree->right == 0))
	{
		trie[(tree->c)] = code;
	}
	//if there is a left child, add 0 to the code and move to the left
	if(tree->left != 0)
	{
		code += "0";
		tree2trie(tree->left, trie, code);
	}
	//if there is a right child, add 1 to the code and move to the right
	if(tree->right != 0)
	{
		code += "1";
		tree2trie(tree->right, trie, code);
	}
	//before returning, remove the last code digit.
	//only if this isn't the root node.
	if (code.length() != 0)
		code = code.substr(0, code.length() - 1);
}

void file2trie(map<std::string, char>& trie)
{
	//a string to hold the code for processing
	//and another to get each line from trie file(stdin)
	std::string code, line;
	bool endofcode;
	while (getline(cin, line))
	{
		//as soon as a tab is encountered, the code has ended
		//and the next character is the character mapped to that code
		endofcode = false;
		code = "";
		//add each character on the line until a tab is encountered
		for (int i = 0; i < line.length() && !endofcode; i++)
		{
			if (line[i] == '\t') endofcode = true;
			else code += line[i];
		}
		//if this is a code
		if (code.length() != 0)
		{
			//handle newlines differently. Have to handle tabs as well as a result
			//for all other characters, place them in the corresponding map index
			if (line[line.length() - 1] == '\t')
			{
				//a tab is also tab delimited, but a newline just has a tab at the end
				//getline() doesn't include the newline.
				if (line[line.length() - 2] == '\t') trie[code] = '\t';
				else trie[code] = '\n';
			}
			else trie[code] = line[line.length() - 1];
		}
	}
}

void hcompress(char* data, int numchar, map<char, std::string>& trie)
{
	//holds bit string until it is 8 characters long
	string binStr = "";
	//holds 8 bits from the character string to output to stdout (now file)
	unsigned char b8;
	int percent = 0;
	//encode every character
	for(int i=0; i<numchar; i++)
	{
		//append trie[data[i]] to current bit string
		binStr += trie[data[i]];
		//when bit string is longer than 8 characters, start outputing encoded bytes
		while(binStr.length() >= 8)
		{
			//convert first 8 characters to binary for a one byte character
			//output byte
			//remove the first 8 characters from string
			b8 = std::stol(binStr.substr(0,8), 0, 2);
			cout<<b8;
			binStr = binStr.substr(8);
		}
		//output percent completed at every percent
		if (percent == i * 100 / numchar) std::cerr << percent++ << "%\n";
	}

	//handle last code. Should be "\0"
	if (binStr.length() != 0){
		//pad with zeros to the right and output the last byte
		string zeros = "00000000";
		binStr += zeros.substr(0, (8 -(binStr.length() % 8)));
		b8 = std::stol(binStr, 0, 2);
		cout << b8;
	}
}

void dcompress(char* data, int numchar, map<std::string, char>& trie)
{
	//string to hold code
	std::string code = "";
	//end of file flag (set when huffman code for null is encountered
	bool eof = false;
	int percent = 0;
	//reading byte by byte
	for(int i=0; i<numchar && !eof; i++)
	{
		//get each bit from byte i one at a time
		for (int j = 7; j >= 0 && !eof; j--)
		{
			code += (((data[i] >> j) & 1) ? "1" : "0");
			//if we have a valid code
			if (trie.find(code.c_str()) != trie.end())
			{
				eof = (trie[code] == '\0');
				//if null code is still not found, output the character for this code
				if (!eof)
					cout << trie[code];
				//clear the code
				code = "";
			}
		}
		//display percent completed
		if (percent == i * 100 / numchar) std::cerr << percent++ << "%\n";
	}
}