#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;

#include "knapsack.h"

/*
 *main() expects a file to replace stdin
 *The file is formated like this:
 *
 *maxCapacity
 *profit0 weight0
 *profit1 weight1
 *.
 *.
 *.
 */
int main()
{

	//I couldn't just declare this in the header ;_;
	knapsack knapsack;

	int p, w, maxW, maxP, size;
	vector<int> profits, weights;
	
	//read in the problem file
	cin >> maxW;
	while (cin >> p)
	{
		cin >> w;
		profits.push_back(p);
		weights.push_back(w);
	}


	size = profits.size();
	//allocate space for the solution set
	bool *bestset = new bool[size];

	//solve 0-1 knapsack and get max profit
	maxP = knapsack(&weights[0], &profits[0], size, maxW, bestset);

	//output results to stdout
	cout << "\n\tIn the napsack, we have:\n"
		<< "\tProfit\tWeight\n"
		<< "\t------\t------\n\n";
	for (int i = 0; i < size; i++)
		if (bestset[i]) cout << "\t$" << profits[i] << "\t" << weights[i] << "\n";
	cout << "\tTotal profit: $" << maxP << "\n\n";

	delete[] bestset;
	return 0;
}