#pragma once
#include <algorithm>
using std::swap;
//#include<iostream>

class knapsack {
public:

	//the function used to solve knapsack
	//parameters: (weights, profits, size, capacity, bool includeInBestSet[size])
	//return: Integer maxProfit
	//WARNING: weights and profits will be sorted by profits[i]/weights[i] descending
	int operator()(int*, int*, int, int, bool*);

private:
	int *w; //weights
	int *p; //profits
	int n; //size of arrays
	int W; //max capacity
	bool *bestset; //all trues are in the solution set

	bool *include; //temporary location for candidate solution
	int maxProfit; //return value for operator()

	//backtracking algorithm for 0-1 knapsack problem
	//parameters: (index i, profit, weight)
	void zero1(int, int, int);
	
	//returns whether i is a promising item
	//parameters: (index i, profit, weight)
	//return: yes/no
	bool promising(int, int, int);

	//need to sort by p/w. Bringing out the big guns

	//calls modified quicksort
	void sort();
	
	//sorts weights and profits by p/w descending
	//parameters: (lowerBound, upperBound)
	//returns nothing. Member arrays are sorted
	void quicksort(int, int);

	//partitons the list by placing all values greater than the pivot item to the
	//left of it, and everything else to the right
	//parameters: (lowerBound, upperBound)
	//return: Integer pivotIndex
	int partition(int, int);
};//knapsack; //why linker complains?!