#include "knapsack.h"

int knapsack::operator()(int* weights, int* profits, int size, int MaxW, bool* inBest) 
{
	//initialize member variables. This knapsack class makes so much more
	//sense than globally declaring these things as is the textbook's convention
	this->w = weights;
	this->p = profits;
	this->n = size;
	this->W = MaxW;
	this->bestset = inBest;

	//allocate space to store work
	this->include = new bool[this->n];
	this->maxProfit = 0;

	//initialize temp and solution arrays
	for (int i = 0; i < this->n; i++)
	{
		this->include[i] = false;
		this->bestset[i] = false;
	}

	//sort by p/w
	sort();
	//The textbook calls knapsack(0,0,0), but it assumes arrays are index from 1 to n.
	//Since arrays are indexed from 0 to n-1, some slight changes had to be made.
	zero1(-1, 0, 0);

	//free up temp memory and return the results
	delete[] this->include;
	return this->maxProfit;
}

void knapsack::zero1(int i, int profit, int weight)
{
	//printf("%d: %d, %d | %d, %d\n", i, profit, weight, W, maxProfit);

	//if capacity hasn't been exceeded and this solution is better than any
	//previous solution, make this the solution
	if (weight <= this->W && profit > this->maxProfit)
	{
		this->maxProfit = profit;
		for (int j = 0; j < this->n; j++) this->bestset[j] = this->include[j];
	}

	//if it looks like we can add more, lets see what happens
	//if we add the next item or if we don't add the next item
	if (promising(i, profit, weight))
	{
		this->include[i + 1] = true;
		zero1(i + 1, profit + this->p[i + 1], weight + this->w[i + 1]);
		this->include[i + 1] = false;
		zero1(i + 1, profit, weight);
	}
}



bool knapsack::promising(int i, int profit, int weight)
{
	double bound = 0;
	//if the capacity hasn't been exceeded
	if (weight < this->W)
	{
		i++;
		bound = profit;
		//try adding as much as possible to our bag
		while (i < this->n && weight + this->w[i] <= this->W)
		{
			weight += this->w[i];
			bound += this->p[i];
			i++;
		}
		//if there are more items left, what happens if we add a piece of
		//the next item to finish filling up the bag?
		if (i < this->n)
		{
			bound += (this->W - weight)*((double)this->p[i]) / (this->w[i]);
		}
	}
	//printf("%f > %d\n", bound, maxProfit);

	//return whether or not it looks like we can make more profit
	return (bound > this->maxProfit);
}

void knapsack::sort() { quicksort(0, this->n); }
void knapsack::quicksort(int l, int h)
{
	//a fairly standard quicksort()
	if (h > l)
	{
		int p = partition(l, h);
		quicksort(l, p);
		quicksort(p + 1, h);
	}
}
int knapsack::partition(int l, int h)
{
	//a pretty standard partition()
	//the only difference is that it is partitioning 2 arrays according to p[l]/w[l]
	int j;
	double pivotitem = ((double)this->p[l]) / this->w[l];
	j = l;
	for (int i = l + 1; i < h; i++)
	{
		if (((double)this->p[i]) / this->w[i] > pivotitem)
		{
			j++;
			swap(this->p[i], this->p[j]);
			swap(this->w[i], this->w[j]);
		}
	}
	swap(this->p[l], this->p[j]);
	swap(this->w[l], this->w[j]);
	return j;
}